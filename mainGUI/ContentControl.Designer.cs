﻿namespace mainGUI
{
    partial class ccContent
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fpnlWrapper = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlLogin = new System.Windows.Forms.Panel();
            this.lblLogin = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblCompId = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.pnlMainMenu = new System.Windows.Forms.Panel();
            this.btnProjMan = new System.Windows.Forms.Button();
            this.btnHoliday = new System.Windows.Forms.Button();
            this.btnInvoicing = new System.Windows.Forms.Button();
            this.btnPayroll = new System.Windows.Forms.Button();
            this.btnServer = new System.Windows.Forms.Button();
            this.btnHelp = new System.Windows.Forms.Button();
            this.btnRoomBooking = new System.Windows.Forms.Button();
            this.btnAdmin = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnPurchasing = new System.Windows.Forms.Button();
            this.btnAccounts = new System.Windows.Forms.Button();
            this.btnCalendar = new System.Windows.Forms.Button();
            this.btnDigital = new System.Windows.Forms.Button();
            this.btnOurbay = new System.Windows.Forms.Button();
            this.btnInternalcomm = new System.Windows.Forms.Button();
            this.btnStock = new System.Windows.Forms.Button();
            this.pnlHelp = new System.Windows.Forms.Panel();
            this.btnSend = new System.Windows.Forms.Button();
            this.lblIssue = new System.Windows.Forms.Label();
            this.lblSubject = new System.Windows.Forms.Label();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.rtbMessage = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fpnlWrapper.SuspendLayout();
            this.pnlLogin.SuspendLayout();
            this.pnlMainMenu.SuspendLayout();
            this.pnlHelp.SuspendLayout();
            this.SuspendLayout();
            // 
            // fpnlWrapper
            // 
            this.fpnlWrapper.Controls.Add(this.pnlLogin);
            this.fpnlWrapper.Controls.Add(this.pnlMainMenu);
            this.fpnlWrapper.Controls.Add(this.pnlHelp);
            this.fpnlWrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlWrapper.Location = new System.Drawing.Point(0, 0);
            this.fpnlWrapper.Margin = new System.Windows.Forms.Padding(0);
            this.fpnlWrapper.Name = "fpnlWrapper";
            this.fpnlWrapper.Size = new System.Drawing.Size(584, 1740);
            this.fpnlWrapper.TabIndex = 0;
            // 
            // pnlLogin
            // 
            this.pnlLogin.Controls.Add(this.lblLogin);
            this.pnlLogin.Controls.Add(this.btnLogin);
            this.pnlLogin.Controls.Add(this.lblPassword);
            this.pnlLogin.Controls.Add(this.lblCompId);
            this.pnlLogin.Controls.Add(this.txtPassword);
            this.pnlLogin.Controls.Add(this.txtUsername);
            this.pnlLogin.Location = new System.Drawing.Point(0, 0);
            this.pnlLogin.Margin = new System.Windows.Forms.Padding(0);
            this.pnlLogin.Name = "pnlLogin";
            this.pnlLogin.Size = new System.Drawing.Size(584, 580);
            this.pnlLogin.TabIndex = 1;
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogin.Location = new System.Drawing.Point(240, 20);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(172, 38);
            this.lblLogin.TabIndex = 23;
            this.lblLogin.Text = "User Login";
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.SandyBrown;
            this.btnLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.Location = new System.Drawing.Point(332, 313);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(87, 28);
            this.btnLogin.TabIndex = 10;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassword.Location = new System.Drawing.Point(166, 255);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(108, 23);
            this.lblPassword.TabIndex = 9;
            this.lblPassword.Text = "Password:";
            // 
            // lblCompId
            // 
            this.lblCompId.AutoSize = true;
            this.lblCompId.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompId.Location = new System.Drawing.Point(161, 192);
            this.lblCompId.Name = "lblCompId";
            this.lblCompId.Size = new System.Drawing.Size(113, 23);
            this.lblCompId.TabIndex = 8;
            this.lblCompId.Text = "Username:";
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(245, 252);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(174, 31);
            this.txtPassword.TabIndex = 7;
            this.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPassword_KeyDown);
            // 
            // txtUsername
            // 
            this.txtUsername.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsername.Location = new System.Drawing.Point(245, 189);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(174, 31);
            this.txtUsername.TabIndex = 6;
            this.txtUsername.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pnlMainMenu
            // 
            this.pnlMainMenu.Controls.Add(this.btnProjMan);
            this.pnlMainMenu.Controls.Add(this.btnHoliday);
            this.pnlMainMenu.Controls.Add(this.btnInvoicing);
            this.pnlMainMenu.Controls.Add(this.btnPayroll);
            this.pnlMainMenu.Controls.Add(this.btnServer);
            this.pnlMainMenu.Controls.Add(this.btnHelp);
            this.pnlMainMenu.Controls.Add(this.btnRoomBooking);
            this.pnlMainMenu.Controls.Add(this.btnAdmin);
            this.pnlMainMenu.Controls.Add(this.lblTitle);
            this.pnlMainMenu.Controls.Add(this.btnPurchasing);
            this.pnlMainMenu.Controls.Add(this.btnAccounts);
            this.pnlMainMenu.Controls.Add(this.btnCalendar);
            this.pnlMainMenu.Controls.Add(this.btnDigital);
            this.pnlMainMenu.Controls.Add(this.btnOurbay);
            this.pnlMainMenu.Controls.Add(this.btnInternalcomm);
            this.pnlMainMenu.Controls.Add(this.btnStock);
            this.pnlMainMenu.Location = new System.Drawing.Point(0, 580);
            this.pnlMainMenu.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMainMenu.Name = "pnlMainMenu";
            this.pnlMainMenu.Size = new System.Drawing.Size(584, 580);
            this.pnlMainMenu.TabIndex = 0;
            this.pnlMainMenu.Visible = false;
            // 
            // btnProjMan
            // 
            this.btnProjMan.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnProjMan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnProjMan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProjMan.Enabled = false;
            this.btnProjMan.FlatAppearance.BorderSize = 0;
            this.btnProjMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProjMan.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProjMan.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnProjMan.Location = new System.Drawing.Point(192, 421);
            this.btnProjMan.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnProjMan.Name = "btnProjMan";
            this.btnProjMan.Size = new System.Drawing.Size(112, 112);
            this.btnProjMan.TabIndex = 43;
            this.btnProjMan.Tag = "14,projManSystem.exe";
            this.btnProjMan.Text = "Project Management";
            this.btnProjMan.UseVisualStyleBackColor = false;
            this.btnProjMan.Click += new System.EventHandler(this.deptButton_Click);
            // 
            // btnHoliday
            // 
            this.btnHoliday.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnHoliday.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnHoliday.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHoliday.Enabled = false;
            this.btnHoliday.FlatAppearance.BorderSize = 0;
            this.btnHoliday.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHoliday.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHoliday.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnHoliday.Location = new System.Drawing.Point(71, 299);
            this.btnHoliday.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnHoliday.Name = "btnHoliday";
            this.btnHoliday.Size = new System.Drawing.Size(112, 112);
            this.btnHoliday.TabIndex = 42;
            this.btnHoliday.Tag = "9,holiday.exe";
            this.btnHoliday.Text = "Holiday Booking";
            this.btnHoliday.UseVisualStyleBackColor = false;
            this.btnHoliday.Click += new System.EventHandler(this.deptButton_Click);
            // 
            // btnInvoicing
            // 
            this.btnInvoicing.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnInvoicing.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnInvoicing.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnInvoicing.Enabled = false;
            this.btnInvoicing.FlatAppearance.BorderSize = 0;
            this.btnInvoicing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInvoicing.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInvoicing.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnInvoicing.Location = new System.Drawing.Point(307, 177);
            this.btnInvoicing.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnInvoicing.Name = "btnInvoicing";
            this.btnInvoicing.Size = new System.Drawing.Size(112, 112);
            this.btnInvoicing.TabIndex = 41;
            this.btnInvoicing.Tag = "7,invoicing.exe";
            this.btnInvoicing.Text = "Invoicing";
            this.btnInvoicing.UseVisualStyleBackColor = false;
            this.btnInvoicing.Click += new System.EventHandler(this.deptButton_Click);
            // 
            // btnPayroll
            // 
            this.btnPayroll.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnPayroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPayroll.Enabled = false;
            this.btnPayroll.FlatAppearance.BorderSize = 0;
            this.btnPayroll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPayroll.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPayroll.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPayroll.Location = new System.Drawing.Point(307, 55);
            this.btnPayroll.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnPayroll.Name = "btnPayroll";
            this.btnPayroll.Size = new System.Drawing.Size(112, 112);
            this.btnPayroll.TabIndex = 40;
            this.btnPayroll.Tag = "3,payroll.exe";
            this.btnPayroll.Text = "Payroll";
            this.btnPayroll.UseVisualStyleBackColor = false;
            this.btnPayroll.Click += new System.EventHandler(this.deptButton_Click);
            // 
            // btnServer
            // 
            this.btnServer.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnServer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnServer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnServer.Enabled = false;
            this.btnServer.FlatAppearance.BorderSize = 0;
            this.btnServer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnServer.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnServer.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnServer.Location = new System.Drawing.Point(71, 55);
            this.btnServer.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnServer.Name = "btnServer";
            this.btnServer.Size = new System.Drawing.Size(112, 112);
            this.btnServer.TabIndex = 39;
            this.btnServer.Tag = "1,server.exe";
            this.btnServer.Text = "Server";
            this.btnServer.UseVisualStyleBackColor = false;
            this.btnServer.Click += new System.EventHandler(this.deptButton_Click);
            // 
            // btnHelp
            // 
            this.btnHelp.BackColor = System.Drawing.Color.SandyBrown;
            this.btnHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHelp.FlatAppearance.BorderSize = 0;
            this.btnHelp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHelp.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHelp.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnHelp.Location = new System.Drawing.Point(425, 482);
            this.btnHelp.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(112, 51);
            this.btnHelp.TabIndex = 38;
            this.btnHelp.Text = "Help";
            this.btnHelp.UseVisualStyleBackColor = false;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // btnRoomBooking
            // 
            this.btnRoomBooking.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnRoomBooking.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRoomBooking.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRoomBooking.Enabled = false;
            this.btnRoomBooking.FlatAppearance.BorderSize = 0;
            this.btnRoomBooking.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRoomBooking.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRoomBooking.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnRoomBooking.Location = new System.Drawing.Point(189, 299);
            this.btnRoomBooking.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnRoomBooking.Name = "btnRoomBooking";
            this.btnRoomBooking.Size = new System.Drawing.Size(112, 112);
            this.btnRoomBooking.TabIndex = 28;
            this.btnRoomBooking.Tag = "10,roomBooking.exe";
            this.btnRoomBooking.Text = "Room Booking";
            this.btnRoomBooking.UseVisualStyleBackColor = false;
            this.btnRoomBooking.Click += new System.EventHandler(this.deptButton_Click);
            // 
            // btnAdmin
            // 
            this.btnAdmin.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnAdmin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdmin.Enabled = false;
            this.btnAdmin.FlatAppearance.BorderSize = 0;
            this.btnAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdmin.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdmin.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAdmin.Location = new System.Drawing.Point(189, 55);
            this.btnAdmin.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnAdmin.Name = "btnAdmin";
            this.btnAdmin.Size = new System.Drawing.Size(112, 112);
            this.btnAdmin.TabIndex = 37;
            this.btnAdmin.Tag = "2,admin.exe";
            this.btnAdmin.Text = "Admin";
            this.btnAdmin.UseVisualStyleBackColor = false;
            this.btnAdmin.Click += new System.EventHandler(this.deptButton_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(240, 20);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(190, 38);
            this.lblTitle.TabIndex = 36;
            this.lblTitle.Text = "Main Menu";
            // 
            // btnPurchasing
            // 
            this.btnPurchasing.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnPurchasing.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPurchasing.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPurchasing.Enabled = false;
            this.btnPurchasing.FlatAppearance.BorderSize = 0;
            this.btnPurchasing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPurchasing.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPurchasing.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPurchasing.Location = new System.Drawing.Point(71, 177);
            this.btnPurchasing.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnPurchasing.Name = "btnPurchasing";
            this.btnPurchasing.Size = new System.Drawing.Size(112, 112);
            this.btnPurchasing.TabIndex = 30;
            this.btnPurchasing.Tag = "5,purchasing.exe";
            this.btnPurchasing.Text = "Purchasing";
            this.btnPurchasing.UseVisualStyleBackColor = false;
            this.btnPurchasing.Click += new System.EventHandler(this.deptButton_Click);
            // 
            // btnAccounts
            // 
            this.btnAccounts.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnAccounts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAccounts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAccounts.Enabled = false;
            this.btnAccounts.FlatAppearance.BorderSize = 0;
            this.btnAccounts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccounts.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccounts.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAccounts.Location = new System.Drawing.Point(425, 55);
            this.btnAccounts.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnAccounts.Name = "btnAccounts";
            this.btnAccounts.Size = new System.Drawing.Size(112, 112);
            this.btnAccounts.TabIndex = 31;
            this.btnAccounts.Tag = "4,accounts.exe";
            this.btnAccounts.Text = "Accounts";
            this.btnAccounts.UseVisualStyleBackColor = false;
            this.btnAccounts.Click += new System.EventHandler(this.deptButton_Click);
            // 
            // btnCalendar
            // 
            this.btnCalendar.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnCalendar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCalendar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCalendar.Enabled = false;
            this.btnCalendar.FlatAppearance.BorderSize = 0;
            this.btnCalendar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalendar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalendar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCalendar.Location = new System.Drawing.Point(189, 177);
            this.btnCalendar.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnCalendar.Name = "btnCalendar";
            this.btnCalendar.Size = new System.Drawing.Size(112, 112);
            this.btnCalendar.TabIndex = 29;
            this.btnCalendar.Tag = "6,calendar.exe";
            this.btnCalendar.Text = "Calendar";
            this.btnCalendar.UseVisualStyleBackColor = false;
            this.btnCalendar.Click += new System.EventHandler(this.deptButton_Click);
            // 
            // btnDigital
            // 
            this.btnDigital.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnDigital.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnDigital.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDigital.Enabled = false;
            this.btnDigital.FlatAppearance.BorderSize = 0;
            this.btnDigital.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDigital.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDigital.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnDigital.Location = new System.Drawing.Point(425, 299);
            this.btnDigital.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnDigital.Name = "btnDigital";
            this.btnDigital.Size = new System.Drawing.Size(112, 112);
            this.btnDigital.TabIndex = 35;
            this.btnDigital.Tag = "12,digitaldocs.exe";
            this.btnDigital.Text = "Digital Docs";
            this.btnDigital.UseVisualStyleBackColor = false;
            this.btnDigital.Click += new System.EventHandler(this.deptButton_Click);
            // 
            // btnOurbay
            // 
            this.btnOurbay.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnOurbay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnOurbay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOurbay.Enabled = false;
            this.btnOurbay.FlatAppearance.BorderSize = 0;
            this.btnOurbay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOurbay.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOurbay.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnOurbay.Location = new System.Drawing.Point(425, 177);
            this.btnOurbay.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnOurbay.Name = "btnOurbay";
            this.btnOurbay.Size = new System.Drawing.Size(112, 112);
            this.btnOurbay.TabIndex = 32;
            this.btnOurbay.Tag = "8,ourbay.exe";
            this.btnOurbay.Text = "ourBay";
            this.btnOurbay.UseVisualStyleBackColor = false;
            this.btnOurbay.Click += new System.EventHandler(this.deptButton_Click);
            // 
            // btnInternalcomm
            // 
            this.btnInternalcomm.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnInternalcomm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnInternalcomm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnInternalcomm.Enabled = false;
            this.btnInternalcomm.FlatAppearance.BorderSize = 0;
            this.btnInternalcomm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInternalcomm.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInternalcomm.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnInternalcomm.Location = new System.Drawing.Point(71, 421);
            this.btnInternalcomm.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnInternalcomm.Name = "btnInternalcomm";
            this.btnInternalcomm.Size = new System.Drawing.Size(112, 112);
            this.btnInternalcomm.TabIndex = 34;
            this.btnInternalcomm.Tag = "13,internalComms.exe";
            this.btnInternalcomm.Text = "Internal Comm.";
            this.btnInternalcomm.UseVisualStyleBackColor = false;
            this.btnInternalcomm.Click += new System.EventHandler(this.deptButton_Click);
            // 
            // btnStock
            // 
            this.btnStock.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnStock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnStock.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStock.Enabled = false;
            this.btnStock.FlatAppearance.BorderSize = 0;
            this.btnStock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStock.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStock.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnStock.Location = new System.Drawing.Point(307, 299);
            this.btnStock.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnStock.Name = "btnStock";
            this.btnStock.Size = new System.Drawing.Size(112, 112);
            this.btnStock.TabIndex = 33;
            this.btnStock.Tag = "11,stock.exe";
            this.btnStock.Text = "Stock";
            this.btnStock.UseVisualStyleBackColor = false;
            this.btnStock.Click += new System.EventHandler(this.deptButton_Click);
            // 
            // pnlHelp
            // 
            this.pnlHelp.Controls.Add(this.btnSend);
            this.pnlHelp.Controls.Add(this.lblIssue);
            this.pnlHelp.Controls.Add(this.lblSubject);
            this.pnlHelp.Controls.Add(this.txtSubject);
            this.pnlHelp.Controls.Add(this.rtbMessage);
            this.pnlHelp.Controls.Add(this.label1);
            this.pnlHelp.Location = new System.Drawing.Point(0, 1160);
            this.pnlHelp.Margin = new System.Windows.Forms.Padding(0);
            this.pnlHelp.Name = "pnlHelp";
            this.pnlHelp.Size = new System.Drawing.Size(584, 580);
            this.pnlHelp.TabIndex = 2;
            this.pnlHelp.Visible = false;
            // 
            // btnSend
            // 
            this.btnSend.BackColor = System.Drawing.Color.SandyBrown;
            this.btnSend.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSend.FlatAppearance.BorderSize = 0;
            this.btnSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSend.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSend.Location = new System.Drawing.Point(355, 443);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(87, 29);
            this.btnSend.TabIndex = 27;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = false;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // lblIssue
            // 
            this.lblIssue.AutoSize = true;
            this.lblIssue.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIssue.Location = new System.Drawing.Point(127, 221);
            this.lblIssue.Name = "lblIssue";
            this.lblIssue.Size = new System.Drawing.Size(74, 23);
            this.lblIssue.TabIndex = 26;
            this.lblIssue.Text = "Query:";
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubject.Location = new System.Drawing.Point(117, 177);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(87, 23);
            this.lblSubject.TabIndex = 25;
            this.lblSubject.Text = "Subject:";
            // 
            // txtSubject
            // 
            this.txtSubject.Location = new System.Drawing.Point(185, 177);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(257, 28);
            this.txtSubject.TabIndex = 24;
            this.txtSubject.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // rtbMessage
            // 
            this.rtbMessage.Location = new System.Drawing.Point(185, 219);
            this.rtbMessage.Name = "rtbMessage";
            this.rtbMessage.Size = new System.Drawing.Size(257, 205);
            this.rtbMessage.TabIndex = 23;
            this.rtbMessage.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(240, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 38);
            this.label1.TabIndex = 22;
            this.label1.Text = "Help Menu";
            // 
            // ccContent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.Controls.Add(this.fpnlWrapper);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ccContent";
            this.Size = new System.Drawing.Size(584, 1740);
            this.Load += new System.EventHandler(this.ccContent_Load);
            this.fpnlWrapper.ResumeLayout(false);
            this.pnlLogin.ResumeLayout(false);
            this.pnlLogin.PerformLayout();
            this.pnlMainMenu.ResumeLayout(false);
            this.pnlMainMenu.PerformLayout();
            this.pnlHelp.ResumeLayout(false);
            this.pnlHelp.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnRoomBooking;
        private System.Windows.Forms.Button btnAdmin;
        private System.Windows.Forms.Button btnCalendar;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnPurchasing;
        private System.Windows.Forms.Button btnAccounts;
        private System.Windows.Forms.Button btnDigital;
        private System.Windows.Forms.Button btnOurbay;
        private System.Windows.Forms.Button btnInternalcomm;
        private System.Windows.Forms.Button btnStock;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblCompId;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Label lblIssue;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.RichTextBox rtbMessage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblLogin;
        public System.Windows.Forms.FlowLayoutPanel fpnlWrapper;
        public System.Windows.Forms.Panel pnlMainMenu;
        public System.Windows.Forms.Panel pnlLogin;
        public System.Windows.Forms.Panel pnlHelp;
        public System.Windows.Forms.Button btnHelp;
        private System.Windows.Forms.Button btnServer;
        private System.Windows.Forms.Button btnPayroll;
        private System.Windows.Forms.Button btnInvoicing;
        private System.Windows.Forms.Button btnHoliday;
        private System.Windows.Forms.Button btnProjMan;
    }
}
