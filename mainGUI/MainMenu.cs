﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OrinocoAccess;
namespace mainGUI
{
    public partial class frmMainMenu : Form
    {
        private ccContent mainContent;

        public frmMainMenu()
        {
            InitializeComponent();
            mainContent = new ccContent();
            mainContent.Parent = this;
            mainContent.Show();
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void Update_UI()
        {
            if(ccMainContent.pnlLogin.Visible == true)
            {
                btnLogout.Visible = false;
            }
            else
            {
                btnLogout.Visible = true;
            }

            if(ccMainContent.pnlHelp.Visible == true)
            {
                btnMainMenu.Visible = true;
            }
            else
            {
                btnMainMenu.Visible = false;
            }

        }

        private void btnMainMenu_Click(object sender, EventArgs e)
        {
            ccMainContent.pnlLogin.Visible = false;
            ccMainContent.pnlHelp.Visible = false;
            ccMainContent.pnlMainMenu.Visible = true;

            Update_UI();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            ccMainContent.pnlLogin.Visible = true;
            ccMainContent.pnlHelp.Visible = false;
            ccMainContent.pnlMainMenu.Visible = false;


            // Loop through the controls on the display panel
            foreach (Control cntrl in ccMainContent.pnlMainMenu.Controls)
            {
                // If the control is a button
                // And the button has a tag
                if (cntrl is Button && cntrl.Tag != null)
                {
                    // Disable the button
                    cntrl.Enabled = false;             
                }
            }

            Update_UI();
        }

    }
}
