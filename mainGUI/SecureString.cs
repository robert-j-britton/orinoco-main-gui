﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mainGUI
{
    public static class SecureString
    {
        /// <summary>
        /// Encrypt passed string.
        /// </summary>
        /// <param name="inString">string</param>
        /// <returns>string</returns>
        public static string Encrypt(string inString)
        {
            string result = "";
            int chrValue = 0;

            foreach (char chr in inString)
            {
                chrValue = chr;
                result += (char)++chrValue;
            }

            return result;
        }

        /// <summary>
        /// Decrypt passed string.
        /// </summary>
        /// <param name="inString">string</param>
        /// <returns>string</returns>
        public static string Decrypt(string inString)
        {
            string result = "";
            int chrValue = 0;

            foreach (char chr in inString)
            {
                chrValue = chr;
                result += (char)--chrValue;
            }

            return result;
        }
    }
}
